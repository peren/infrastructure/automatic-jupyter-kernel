# Automatic-jupyter-kernel

This package add an "automatic_kernel" kernel in jupyter that you can select to automatically use the closest venv from your notebook.
As we can't install it on user wide system, you will need to setup a virtual env for it

## Installation

```
python -m venv .venv
.venv/bin/pip install ipykernel
.venv/bin/pip install -e .
```


## Usage

In the notebook, choose "automatic_kernel" kernel as your kernel, and it will use the closest (in the notebook folder or its parent) venv and use it.

## How does it work ?

The installation procedure will install in your jupyter namespace (`~/.local/share/jupyter/kernels`) 
a new environnement that, instead of directly launching a python environnement, will launch a script (the `__main__` of [automatic_jupyter_kernel](automatic_jupyter_kernel))
that will :
- find the closest `.venv` to where the notebook is (either in the same directory of the notebook or its parent), and create one if it doesn't exist
- install `ipykernel` in it (if `.is_installed` is not present in `.venv/bin`)
- run the venv

# Credits

Strongly inspired from https://github.com/pathbird/poetry-kernel